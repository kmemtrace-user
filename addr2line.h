/*
 * Copyright (C) 2008 Eduard-Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#ifndef _ADDR2LINE_H
#define _ADDR2LINE_H

extern int addr2line_init(const char *vmlinux_path);
extern void addr2line_exit(void);
extern ssize_t addr2line_query(void *addr, char *result, size_t len); 

#endif

