/*
 * Copyright (C) 2008 Eduard-Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

/*
 * Simple hashtable for storing pointer-sized keyed data.
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "hashtable.h"

struct ht_table *__ht_create(size_t bits, size_t entry_size, size_t key_offset)
{
	struct ht_table *ret;
	size_t n_entries = 1 << bits;
	
	ret = malloc(sizeof(struct ht_table));
	if (!ret)
		return NULL;
	ret->entry = calloc(n_entries, sizeof(struct ht_entry));
	ret->bits = bits;
	ret->entry_size = entry_size;
	ret->n_entries = 0;
	ret->n_entries_root = 0;
	ret->key_offset = key_offset;

	return ret;
}

void ht_destroy(struct ht_table *table)
{
	free(table->entry);
	free(table);
}

static inline unsigned long ht_hash(uint64_t key, size_t bits)
{
	unsigned long ret = (unsigned long) key;
	unsigned long mask = (1 << bits) - 1;

	return (ret ^ (ret >> 16)) & mask;
}

static inline uint64_t *ht_get_key_ptr(struct ht_entry *entry,
				       struct ht_table *table)
{
	return (uint64_t *) (entry->data + table->key_offset);
}

static inline void ht_set_key(struct ht_entry *entry,
			      struct ht_table *table,
			      uint64_t key)
{
	*ht_get_key_ptr(entry, table) = key;
}

static inline uint64_t ht_get_key(struct ht_entry *entry,
				  struct ht_table *table)
{
	return *ht_get_key_ptr(entry, table);
}

struct ht_entry *ht_update_entry(struct ht_table *table, uint64_t key)
{
	struct ht_entry *entry;
	unsigned long bucket = ht_hash(key, table->bits);

	entry = &table->entry[bucket];
	if (!entry->data) {
		entry->data = calloc(1, table->entry_size);
		ht_set_key(entry, table, key);
		table->n_entries++;
		table->n_entries_root++;
		return entry;
	} else {
		while (entry) {
			if (ht_get_key(entry, table) == key)
				return entry;
			if (!entry->next) {
				entry->next =
					calloc(1, sizeof(struct ht_entry));
				entry->next->data =
					calloc(1, table->entry_size);
				ht_set_key(entry->next, table, key);
				table->n_entries++;
				
				return entry->next;
			}

			entry = entry->next;
		}
	}

	return NULL;
}

void ht_action(struct ht_table *table,
	       void (*action)(struct ht_entry *, int, void *),
	       void *private)
{
	unsigned long i;
	struct ht_entry *entry, *next_entry;

	for (i = 0; i < (1 << table->bits); i++) {
		entry = &table->entry[i];
		if (entry->data) {
			next_entry = entry->next;
			action(entry, 0, private);
			entry = next_entry;
		} else
			continue;
		while (entry) {
			next_entry = entry->next;
			action(entry, 1, private);
			entry = next_entry;
		}
	}
}

void ht_free_entry(struct ht_entry *entry, int freeable, void *private)
{
	free(entry->data);
	if (freeable)
		free(entry);
}

struct ht_to_vec_private {
	void *entry;
	size_t idx;
	size_t entry_size;
};

static void ht_to_vec_action(struct ht_entry *entry,
			     int freeable,
			     void *private)
{
	struct ht_to_vec_private *p = private;
	void **t = p->entry + (p->idx++ * sizeof(void *));

	*t = entry->data;
}

static void ht_to_vec_destroy_action(struct ht_entry *entry,
				     int freeable,
				     void *private)
{
	struct ht_to_vec_private *p = private;

	memcpy(p->entry + p->entry_size * p->idx++,
	       entry->data, p->entry_size);
	ht_free_entry(entry, freeable, NULL);
}

size_t ht_to_vec(struct ht_table *table, int destroy, void *vec)
{
	struct ht_to_vec_private priv;

	priv.entry = vec;
	priv.idx = 0;
	priv.entry_size = table->entry_size;

	if (destroy) {
		ht_action(table, ht_to_vec_destroy_action, &priv);
		ht_destroy(table);
	} else
		ht_action(table, ht_to_vec_action, &priv);

	return table->n_entries;
}

