/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#include <common.h>
#include <kmemtrace.h>
#include <dataset.h>

unsigned long ev_num = 1, target_ev;

static int show(struct kmemtrace_event *ev, void *extra)
{
	struct kmemtrace_allocstats *stats = extra;

	if (ev_num != target_ev) {
		ev_num++;
		return 0;
	}

	printf("Event %lu:\n", ev_num);
	printf("\tevent_id\t%d\n\tkind_id\t\t%d\n\tevent_size\t0x%x\n"
	       "\tcall_site\t%p\n\tptr\t\t%p\n\tseq_num\t%ld\n",
	       ev->event_id, ev->type_id, (unsigned int) ev->event_size,
	       (void *) ev->call_site, (void *) ev->ptr, (long) ev->seq_num);
	
	if (ev->event_id == KMEMTRACE_EVENT_ALLOC) {
		if (ev->event_size != sizeof(struct kmemtrace_event) +
				      sizeof(struct kmemtrace_allocstats)) {
			printf("Size of alloc event is wrong!\n");
			return 1;
		}
		printf("\n\tbytes_req\t%lu\n\tbytes_alloc\t%lu\n"
		       "\tgfp_flags\t%d\n\tnuma_node\t%d\n",
		       (unsigned long) stats->bytes_req,
		       (unsigned long) stats->bytes_alloc,
		       (int) stats->gfp_flags, (int) stats->numa_node);
	}

	return 1;
}

int main(int argc, char **argv)
{
	struct dataset *set;
	unsigned long cpu;

	if (argc != 3)
		panic("There must be two arguments!\n");

	cpu = strtoul(argv[1], NULL, 10);
	set = dataset_open("cpu%lu.out", cpu);
	if (!set)
		panic("Could not open input file!\n");
	
	target_ev = strtoul(argv[2], NULL, 10);

	if (!dataset_iter(set, show))
		printf("Not found!\n");

	return 0;
}

