/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#ifndef _KMEMTRACE_H
#define _KMEMTRACE_H

#include <stdint.h>

#define KMEMTRACE_ABI_VERSION		1

enum kmemtrace_event_id {
	KMEMTRACE_EVENT_ALLOC = 0,
	KMEMTRACE_EVENT_FREE,
};

enum kmemtrace_type_id {
	KMEMTRACE_TYPE_KMALLOC = 0,	/* kmalloc() / kfree(). */
	KMEMTRACE_TYPE_CACHE,		/* kmem_cache_*(). */
	KMEMTRACE_TYPE_PAGES,		/* __get_free_pages() and friends. */
};

struct kmemtrace_event {
	uint8_t		event_id;
	uint8_t		type_id;
	uint16_t	event_size;
	int32_t		seq_num;
	uint64_t	call_site;
	uint64_t	ptr;
} __attribute__ ((__packed__));

struct kmemtrace_allocstats {
	uint64_t	bytes_req;
	uint64_t	bytes_alloc;
	uint32_t	gfp_flags;
	int32_t		numa_node;
} __attribute__ ((__packed__));

#endif

