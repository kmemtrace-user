/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <addr2line.h>
#include <config.h>

static int our_pipe[2], a2l_pipe[2]; /* "Ownership" defined by who reads. */
static int addr2line_running;

int addr2line_init(const char *vmlinux_path)
{
	char tmp[500];

	pid_t pid;
	
	if (addr2line_running)
		return -EEXIST;

	if (pipe(our_pipe) < 0)
		goto err;
	if (pipe(a2l_pipe) < 0)
		goto err;

	pid = fork();
	if (pid < 0)
		goto err;
	else if (!pid) {
		close(0);
		dup(a2l_pipe[0]);
		close(1);
		dup(our_pipe[1]);
		sprintf(tmp, "--exe=%s", vmlinux_path);
		exit(execl(ADDR2LINE_PATH, ADDR2LINE_PATH,
			   "-i", tmp, (char *) NULL));
	} else {
		addr2line_running = 1;
	}

	return 0;

err:
	return -1;
}

void addr2line_exit(void)
{
	close(a2l_pipe[1]);
	close(our_pipe[0]);
}

ssize_t addr2line_query(void *addr, char *result, size_t len) 
{
	char ptr_str[32];
	size_t ptr_str_len;
	ssize_t count;

	sprintf(ptr_str, "%p\n", addr);
	ptr_str_len = strlen(ptr_str);
	count = write(a2l_pipe[1], ptr_str, ptr_str_len);
	if (count < ptr_str_len)
		return -EIO;
	
	count = read(our_pipe[0], result, len - 1);
	result[count - 1] = '\0'; /* addr2line terminates lines with '\n' */

	return count;
}

