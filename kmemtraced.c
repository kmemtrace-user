/*
 * Copyright (C) 2008 Pekka Enberg, Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pthread.h>
#include <poll.h>
#include <sched.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/sendfile.h>
#include <unistd.h>

#include "common.h"
#include "kmemtrace.h"

static volatile int terminate;

static void write_str(const char *filename, const char *value)
{
	int fd;

	fd = open(filename, O_RDWR);
	if (fd < 0)
		panic("Could not open() file %s: %s\n", filename, strerror(errno));

	if (write(fd, value, strlen(value)) < 0)
		panic("Could not write() to file %s: %s\n", filename, strerror(errno));

	close(fd);
}

static int open_channel(int cpu)
{
	char filename[PATH_MAX];
	int fd;

	sprintf(filename, "/sys/kernel/debug/kmemtrace/cpu%d", cpu);
	fd = open(filename, O_RDONLY | O_NONBLOCK);
	if (fd < 0)
		panic("Could not open() file %s: %s\n", filename, strerror(errno));
	return fd;
}

static int open_log(int cpu)
{
	char filename[PATH_MAX];
	int fd;

	sprintf(filename, "cpu%d.out", cpu);
	fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC,
		  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fd < 0)
		panic("Could not open() file %s: %s\n",
		      filename, strerror(errno));
	
	return fd;
}

static void reader_set_affinity(unsigned long cpu)
{
	int err;
	cpu_set_t cpumask;

	CPU_ZERO(&cpumask);
	CPU_SET(cpu, &cpumask);
	err = sched_setaffinity(syscall(SYS_gettid),
				sizeof(cpu_set_t), &cpumask);

	if (err == -1)
		panic("reader_set_affinity: %s\n", strerror(errno));
}

static void *reader_thread(void *data)
{
	unsigned long cpu = (unsigned long) data;
	int relay_fd, log_fd;
	int pipe_fd[2];
	long retval;

	reader_set_affinity(cpu);
	
	relay_fd = open_channel(cpu);
	log_fd = open_log(cpu);

	if (pipe(pipe_fd))
		panic("pipe() failed: %s\n", strerror(errno));

	do {
		/*
		 * We don't strip extra features (due to ABI changes) from
		 * events; do that when actually parsing the data.
		 */
		retval = splice(relay_fd, NULL, pipe_fd[1], NULL,
				128, SPLICE_F_MOVE);
		if (retval < 0)
			panic("splice() (from) failed: %s\n",
			      strerror(errno));
		if (!retval)
			continue;
		retval = splice(pipe_fd[0], NULL, log_fd, NULL,
				128, SPLICE_F_MOVE);
		if (retval < 0)
			panic("splice() (to) failed: %s\n", strerror(errno));
	} while (!terminate);

	return NULL;
}

static void copy_kallsyms(void)
{
	int in_fd, out_fd;
	char buf[256];
	ssize_t count;

	in_fd = open("/proc/kallsyms", O_RDONLY);
	if (in_fd == -1)
		goto err;

	out_fd = open("kallsyms", O_CREAT | O_WRONLY | O_TRUNC,
	              S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (out_fd == -1) {
		close(in_fd);
		goto err;
	}

	while ((count = read(in_fd, buf, 256)) > 0)
		write(out_fd, buf, count);
	if (count < 0)
		goto err;

	return;

err:
	panic("copy_kallsyms: %s\n", strerror(errno));
}

int main(int argc, char *argv[])
{
	unsigned long nr_cpus;
	pthread_t *readers;
	sigset_t signals;
	unsigned long i;
	int signal;

	sigemptyset(&signals);
	sigaddset(&signals, SIGINT);
	sigaddset(&signals, SIGTERM);
	pthread_sigmask(SIG_BLOCK, &signals, NULL);

	nr_cpus = sysconf(_SC_NPROCESSORS_ONLN);

	readers = calloc(nr_cpus, sizeof(pthread_t));
	if (!readers)
		panic("Out of memory!\n");

	printf("Copying /proc/kallsyms...\n");
	copy_kallsyms();

	for (i = 0; i < nr_cpus; i++) {
		int err;

		err = pthread_create(&readers[i], NULL, reader_thread,
				     (void *) i);
		if (err)
			panic("Could not pthread_create(): %s!\n",
			      strerror(errno));
	}

	write_str("/sys/kernel/debug/kmemtrace/enabled", "1");

	printf("Logging... Press Control-C to stop.\n");

	while (sigwait(&signals, &signal) == 0) {
		if (signal == SIGINT || signal == SIGTERM)
			/*
			 * No synchronization needed, we wait for
			 * threads to end.
			 */
			terminate = 1;
			break;
	}

	write_str("/sys/kernel/debug/kmemtrace/enabled", "0");

	for (i = 0; i < nr_cpus; i++)
		pthread_join(readers[i], NULL);
	
	return EXIT_SUCCESS;
}
