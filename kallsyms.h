/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#ifndef _KALLSYMS_H
#define _KALLSYMS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define SYMBOL_NAME_MAX		128

struct kallsyms_symbol {
	uint64_t	addr;
	char		*name;
	size_t		name_len;
};

extern int kallsyms_init(const char *path);
extern void kallsyms_exit(void);
extern struct kallsyms_symbol *kallsyms_lookup(uint64_t addr);

#endif
