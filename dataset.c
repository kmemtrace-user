/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include <dataset.h>
#include <common.h>
#include <kmemtrace.h>

struct dataset *dataset_open(char *format, unsigned long cpu)
{
	struct stat stat_buf;
	struct dataset *ret;
	int len;

	ret = malloc(sizeof(struct dataset));
	if (!ret)
		return NULL;

	len = snprintf(ret->filename, FILENAME_MAX, format, cpu);
	if (len >= FILENAME_MAX)
		goto fail;

	ret->fd = open(ret->filename, O_RDONLY);
	if (ret->fd == -1)
		goto fail;

	if (fstat(ret->fd, &stat_buf))
		goto fail_close;
	ret->size = stat_buf.st_size;

	ret->map = mmap(NULL, ret->size, PROT_READ, MAP_PRIVATE, ret->fd, 0);
	if (!ret->map)
		goto fail_close;

	return ret;

fail_close:
	close(ret->fd);
fail:
	free(ret);
	return NULL;
}

void dataset_close(struct dataset *set)
{
	if (!set)
		return;
	
	munmap(set->map, set->size);
	free(set);
}

static inline struct dataset_event dataset_event_read(struct dataset *set,
						      size_t *offset)
{
	struct dataset_event d_ev;

	d_ev.event = set->map + *offset;

	if (((void *) d_ev.event) + d_ev.event->event_size >
	    set->map + set->size) {
		printf("dataset_event_read: Event reaches "
		       "outside mapped region, clamping dataset.\n");
		*offset = set->size;
		d_ev.event = NULL;
		return d_ev;
	}

	if (set->size - *offset < sizeof(struct kmemtrace_event)) {
		panic("dataset_event_read: Not enough data\n");
	}

	if (d_ev.event->event_size > sizeof(struct kmemtrace_event)) {
		d_ev.extra = set->map + *offset +
			     sizeof(struct kmemtrace_event);
	} else
		d_ev.extra = NULL;
	
	*offset += d_ev.event->event_size;

	return d_ev;
}

int dataset_iter(struct dataset *set, int (*action)(struct kmemtrace_event *,
						    void *))
{
	size_t off = 0;
	struct dataset_event d_ev;

	while (off < set->size) {
		d_ev = dataset_event_read(set, &off);
		if (!d_ev.event)
			return 0;
		if (action(d_ev.event, d_ev.extra))
			return -EINTR;
	}

	return 0;
}

static inline unsigned long
dataset_group_get_earliest(struct dataset_group *group)
{
	long i, min = -1;

	for (i = 0; i < group->count; i++) {
		if (!group->has_data[i])
			continue;
		if (min == -1) {
			min = i;
			continue;
		}
		if (group->ds_event[i].event->seq_num <
		    group->ds_event[min].event->seq_num)
			min = i;
	}

	if (min != -1)
		group->has_data[min] = 0;
	
	return min;
}

/* Returns non-zero if interrupted / not completed. */
int dataset_iter_ordered(struct dataset_group *group,
			 int (*action)(struct kmemtrace_event *,
				       void *, unsigned long))
{
	unsigned long i, next;
	size_t *off, prev_off, tsize = 0;
	int32_t expected;
	int have_expected = 0;

	if (!action)
		return -EINVAL;

	off = calloc(group->count, sizeof(size_t));

	for (i = 0; i < group->count; i++)
		tsize += group->dataset[i]->size;
	
	while (tsize > 0) {
		for (i = 0; i < group->count; i++)
			if (off[i] < group->dataset[i]->size &&
			    !group->has_data[i]) {
				prev_off = off[i];
				group->ds_event[i] =
					dataset_event_read(group->dataset[i],
							   &off[i]);
				if (group->ds_event[i].event)
					group->has_data[i] = 1;
				tsize -= off[i] - prev_off;
			}
		
		next = dataset_group_get_earliest(group);

		if (next == -1)
			break;

		if (!have_expected) {
			have_expected = 1;
			expected = group->ds_event[next].event->seq_num + 1;
		} else {
			if (expected != group->ds_event[next].event->seq_num) {
				printf("dataset_iter_ordered: Wrong sequence "
				       "number %d, expected %d.\n",
				       group->ds_event[next].event->seq_num,
				       expected);
				free(off);
				return -EINVAL;
			}
			expected++;
		}

		debug("Processing packet seq_num = %d from CPU %lu...\n",
		      group->ds_event[next].event->seq_num - 1, next);

		if (action(group->ds_event[next].event,
			   group->ds_event[next].extra, next)) {
			free(off);
			return -EINTR;
		}
	}

	free(off);

	return 0;
}

struct dataset_group *dataset_group_create(unsigned long count)
{
	struct dataset_group *ret;

	ret = malloc(sizeof(struct dataset_group));
	if (!ret)
		return NULL;
	ret->dataset = calloc(count, sizeof(struct dataset *));
	ret->ds_event = calloc(count, sizeof(struct dataset_event));
	ret->has_data = calloc(count, sizeof(int));
	ret->count = count;

	return ret;
}

void dataset_group_destroy(struct dataset_group *group)
{
	unsigned long cpu;

	for (cpu = 0; cpu < group->count; cpu++)
		if (group->dataset[cpu])
			dataset_close(group->dataset[cpu]);
	free(group->dataset);
	free(group->ds_event);
	free(group->has_data);

	free(group);
}

