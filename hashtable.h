/*
 * Copyright (C) 2008 Eduard-Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#ifndef _HASHTABLE_H
#define _HASHTABLE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct ht_entry {
	void *data;
	struct ht_entry *next;
};

struct ht_table {
	struct ht_entry	*entry;
	size_t		bits;
	size_t		entry_size;
	size_t		n_entries;
	size_t		n_entries_root;
	size_t		key_offset;
};

#define member_offset(container, member) \
	({ container ctr; (size_t) &ctr.member - (size_t) &ctr; })

#define ht_create(bits, ctr, member) \
	__ht_create(bits, sizeof(ctr), member_offset(ctr, member))
extern struct ht_table *__ht_create(size_t bits,
				    size_t entry_size,
				    size_t key_offset);
extern void ht_destroy(struct ht_table *table);
extern struct ht_entry *ht_update_entry(struct ht_table *table, uint64_t key);
extern void ht_action(struct ht_table *table,
		      void (*action)(struct ht_entry *, int, void *),
		      void *private);
extern void ht_free_entry(struct ht_entry *entry,
			  int freeable,
			  void *private);
extern size_t ht_to_vec(struct ht_table *table, int destroy, void *vec);

static inline void *ht_malloc_vec(struct ht_table *table, int destroy)
{
	void *ret;

	if (destroy)
		ret = malloc(table->n_entries * table->entry_size);
	else
		ret = malloc(table->n_entries * sizeof(void *));

	return ret;
}

static inline void ht_free_vec(void *vec)
{
	free(vec);
}

static inline void ht_show_stats(const char *name,
				 const struct ht_table *table)
{
	printf("%s: %zu/%zu roots, %zu entries\n", name,
	       table->n_entries_root, (size_t) 1 << table->bits,
	       table->n_entries);
}

#endif /* _HASHTABLE_H */

