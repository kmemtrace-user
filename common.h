/*
 * Copyright (C) 2008 Pekka Enberg
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#ifndef _COMMON_H
#define _COMMON_H

extern int debug_enabled;

extern void debug(const char *fmt, ...);
extern void panic(const char *fmt, ...);

#endif /* _COMMON_H */

