/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include <kallsyms.h>

static struct kallsyms_symbol *kallsyms_array;
static unsigned long kallsyms_count;

static int kallsyms_compare_sort(const void *a, const void *b)
{
	return ((const struct kallsyms_symbol * const) a)->addr -
	       ((const struct kallsyms_symbol * const) b)->addr;
}

static unsigned long kallsyms_count_syms(void *map, size_t size)
{
	unsigned long count = 0;
	void *last_map = map;

	while ((last_map = memchr(map, '\n', size))) {
		last_map++;
		size -= last_map - map;
		map = last_map;
		count++;
	}

	return count;
}

static struct kallsyms_symbol *kallsyms_read_syms(void *map, size_t size)
{
	struct kallsyms_symbol *ret;
	char *name_end;
	unsigned long i;

	ret = malloc(kallsyms_count * sizeof(struct kallsyms_symbol));
	if (!ret)
		return NULL;

	for (i = 0; i < kallsyms_count; i++) {
		ret[i].addr = (uint64_t) strtoull((char *) map,
						  (char **) &map, 16);
		map += 3;
		name_end = memchr(map, '\n', SYMBOL_NAME_MAX);
		if (!name_end)
			return NULL;
		ret[i].name_len = (size_t) name_end - (size_t) map + 1;
		ret[i].name = malloc(ret[i].name_len);
		memcpy(ret[i].name, map, ret[i].name_len - 1);
		ret[i].name[ret[i].name_len - 1] = '\0';
		map += ret[i].name_len;
	}

	return ret;
}

int kallsyms_init(const char *path)
{
	int fd;
	size_t size;
	void *map;
	struct stat stat_buf;

	if (kallsyms_array)
		return -EBUSY;
	
	fd = open(path, O_RDONLY);
	if (fd == -1)
		return -EINVAL;

	if (fstat(fd, &stat_buf)) {
		close(fd);
		return -EINVAL;
	}
	size = stat_buf.st_size;

	map = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
	if (!map) {
		close(fd);
		return -EINVAL;
	}

	kallsyms_count = kallsyms_count_syms(map, size);
	kallsyms_array = kallsyms_read_syms(map, size);
	if (!kallsyms_array) {
		close(fd);
		return -ENOMEM;
	}
	qsort(kallsyms_array, kallsyms_count,
	      sizeof(struct kallsyms_symbol), kallsyms_compare_sort);
	
	munmap(map, size);
	close(fd);
	
	return kallsyms_count;
}

void kallsyms_exit(void)
{
	unsigned long i;

	for (i = 0; i < kallsyms_count; i++)
		free(kallsyms_array[i].name);

	free(kallsyms_array);
}

static int kallsyms_compare_lookup(const void *addr_ptr, const void *sym_mb)
{
	const struct kallsyms_symbol * const next =
		(const struct kallsyms_symbol * const)
		sym_mb + 1;
	const struct kallsyms_symbol * const last =
		(const struct kallsyms_symbol * const)
		&kallsyms_array[kallsyms_count - 1];
	const struct kallsyms_symbol * const curr = sym_mb;
	uint64_t addr = *((const uint64_t * const) addr_ptr);

	/* 
	 * We get instruction pointers in *addr_ptr, so we have
	 * found the symbol if its address is equal to or less than addr.
	 */
	if (next <= last) {
		if (curr->addr <= addr && addr < next->addr)
			return 0;
	} else if (addr > curr->addr)
		return 0;

	return addr - curr->addr;
}

struct kallsyms_symbol *kallsyms_lookup(uint64_t addr)
{
	return bsearch(&addr, kallsyms_array, kallsyms_count,
		       sizeof(struct kallsyms_symbol),
		       kallsyms_compare_lookup);
}

