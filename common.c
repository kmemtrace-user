/*
 * Copyright (C) 2008 Pekka Enberg, Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <stdarg.h>

#include "common.h"

int debug_enabled;

void debug(const char *fmt, ...)
{
	va_list args;

	if (debug_enabled) {
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	}
}

void panic(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	exit(EXIT_FAILURE);
}

