/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#include <common.h>
#include <dataset.h>
#include <kmemtrace.h>

int32_t last_timestamp;
unsigned long evt_idx;
struct kmemtrace_event *last_ev;
struct kmemtrace_allocstats *last_stats;

static int check(struct kmemtrace_event *ev, void *extra)
{
	struct kmemtrace_allocstats *stats = extra;

	last_ev = ev;
	last_stats = stats;

	evt_idx++;
	printf("Checking event %lu...\n", evt_idx);

	if (ev->event_id != KMEMTRACE_EVENT_ALLOC &&
	    ev->event_id != KMEMTRACE_EVENT_FREE)
		printf("Bad event with id %d!\n", ev->event_id);
	if (evt_idx != 1 && last_timestamp > ev->seq_num)
		printf("Event is out of order! %lld, last seq_num was %lld.",
		       (long long) ev->seq_num, (long long) last_timestamp);

	if (ev->event_id == KMEMTRACE_EVENT_FREE &&
	    ev->event_size != sizeof(struct kmemtrace_event)) {
		printf("Invalid event size for free.\n");
		return 1;
	}

	if (ev->event_id == KMEMTRACE_EVENT_ALLOC &&
	    (!stats->bytes_req || !stats->bytes_alloc ||
	     (stats->bytes_alloc < stats->bytes_req)))
		printf("Invalid size (%llu, %llu).\n",
		       stats->bytes_req, stats->bytes_alloc);
	last_timestamp = ev->seq_num;

	return 0;
}

int main(int argc, char **argv)
{
	struct dataset *set;
	unsigned long cpu;

	if (argc != 2)
		panic("There is only one argument!\n");

	cpu = strtoul(argv[1], NULL, 10);
	set = dataset_open("cpu%lu.out", cpu);
	if (!set)
		panic("Could not open input file!\n");

	if (dataset_iter(set, check))
		printf("Checking did not complete!\n");

	return 0;
}

