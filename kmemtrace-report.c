/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>
#include <limits.h>

#include <common.h>
#include <kallsyms.h>
#include <hashtable.h>
#include <kmemtrace.h>
#include <dataset.h>
#include <addr2line.h>

#define A2L_QUERY_LEN	512	

struct symbol_stats {
	uint64_t	call_site;	/* Hash key */
	char		*name;
	uint64_t	n_req;
	uint64_t	n_alloc;
	uint64_t	n_freed;
};

#define STILL_ALIVE	INT_MAX
struct allocation_stats {
	uint64_t	call_site;
	char		*name;
	uint64_t	ptr;		/* Hash key */
	uint64_t	n_req;
	uint64_t	n_alloc;
	
	int32_t		born_on;
	int32_t		died_on;
};

static struct ht_table *caller_ht, *alloc_ht;

/* Statistics */
static unsigned long total_requested, total_allocated;
static unsigned long n_cross_allocs, n_cross_frees;
static unsigned long n_ignored;
static int32_t last_timestamp = INT_MIN;

static int callers_flag, allocs_flag, addr2line_flag, debug_flag;
static char vmlinux_path[FILENAME_MAX];

static inline int is_cross_cpu(unsigned long origin, unsigned long target)
{
	return (target != -1 && origin != target);
}

unsigned long *curr_alloc;

static inline void warn_event(struct kmemtrace_event *ev,
			      const char *name, const char *prev_name)
{
	char a2l_result[A2L_QUERY_LEN];

	printf("\tby %s (%llx)\n", name, (unsigned long long) ev->call_site);
	if (addr2line_flag) {
		if (addr2line_query((void *) ev->call_site,
				    a2l_result, A2L_QUERY_LEN) <= 0)
			panic("Couldn't resolve callsite!\n");
		printf("%s\n", a2l_result);
	}
	if (name)
		printf("\tlast touched by %s\n\n", prev_name);
	else
		printf("\tnever seen before!\n\n");
}

static void parse_event_alloc(struct kmemtrace_event *ev,
			      struct kmemtrace_allocstats *evstats,
			      struct symbol_stats *caller_stats,
			      struct allocation_stats *alloc_stats,
			      struct kallsyms_symbol *symbol,
			      unsigned long origin_cpu)
{
	if (alloc_stats) {
		if (alloc_stats->died_on == STILL_ALIVE) {
			printf("Allocation #%lu (CPU%lu) already exists!\n",
			       curr_alloc[origin_cpu], origin_cpu);
			warn_event(ev, symbol->name, alloc_stats->name);
			n_ignored++;
			return;
		}
		
		alloc_stats->call_site = ev->call_site;
		/* alloc_stats->name already stored. */
		/* alloc_stats->ptr automatically stored when registering. */
		alloc_stats->n_req = evstats->bytes_req;
		alloc_stats->n_alloc = evstats->bytes_alloc;
		alloc_stats->born_on = ev->seq_num;
		alloc_stats->died_on = STILL_ALIVE;
	}

	if (caller_stats) {
		caller_stats->n_req += evstats->bytes_req;
		caller_stats->n_alloc += evstats->bytes_alloc;
	}
	
	total_requested += evstats->bytes_req;
	total_allocated += evstats->bytes_alloc;

	if (is_cross_cpu(origin_cpu, evstats->numa_node))
		n_cross_allocs++;
}

static void parse_event_free(struct kmemtrace_event *ev,
			     struct symbol_stats *caller_stats,
			     struct allocation_stats *alloc_stats,
			     struct kallsyms_symbol *symbol,
			     unsigned long origin_cpu)
{
	if (alloc_stats->died_on != STILL_ALIVE) {
		printf("Allocation #%lu (CPU%lu) already dead!\n",
			curr_alloc[origin_cpu], origin_cpu);
		warn_event(ev, symbol->name, alloc_stats->name);
		n_ignored++;
		return;
	}
	
	alloc_stats->died_on = ev->seq_num;
}

static int parse_event(struct kmemtrace_event *ev,
		       void *extra,
		       unsigned long origin_cpu)
{
	struct kallsyms_symbol *sym;
	struct symbol_stats *caller_stats = NULL;
	struct allocation_stats *alloc_stats = NULL;
	size_t name_len;

	if (!ev->ptr)
		return 0;

	curr_alloc[origin_cpu]++;

	sym = kallsyms_lookup(ev->call_site);
	if (!sym) {
		printf("parse_event: Unknown symbol with address %p!\n",
		       (void *) ev->call_site);
		return 1;
	}
	name_len = strlen(sym->name);
	
	if (last_timestamp > ev->seq_num) {
		printf("parse_event: Bad timestamp, caller %s!\n", sym->name);
		return 0;
	}
	last_timestamp = ev->seq_num;

	if (callers_flag) {
		caller_stats = ht_update_entry(caller_ht, ev->call_site)->data;
		if (!caller_stats)
			panic("parse_event: Could not register caller!\n");
		if (!caller_stats->name) {
			caller_stats->name = malloc(name_len + 1);
			strncpy(caller_stats->name, sym->name, name_len + 1);
		}
	}

	alloc_stats = ht_update_entry(alloc_ht, ev->ptr)->data;
	if (!alloc_stats)
		panic("parse_event: Could not register allocation!\n");

	if (ev->event_id == KMEMTRACE_EVENT_ALLOC)
		parse_event_alloc(ev, extra, caller_stats,
				  alloc_stats, sym, origin_cpu);
	else if (ev->event_id == KMEMTRACE_EVENT_FREE)
		parse_event_free(ev, caller_stats,
				 alloc_stats, sym, origin_cpu);
	else
		panic("parse_event: unknown event %d!\n", ev->event_id);
	
	if (!alloc_stats->name) {
		alloc_stats->name = malloc(name_len + 1);
		strncpy(alloc_stats->name, sym->name, name_len + 1);
	}

	return 0;
}
		
static inline float fragmentation(unsigned long n_req, unsigned long n_alloc)
{
	return n_alloc ? (100. - (100. * n_req / n_alloc)) : 0.;
}

static int compare_caller_frag(const void *a, const void *b)
{
	const struct symbol_stats * const sym_a =
		*((const struct symbol_stats **) a);
	const struct symbol_stats * const sym_b =
		*((const struct symbol_stats **) b);
	float frag_a = fragmentation(sym_a->n_req, sym_a->n_alloc);
	float frag_b = fragmentation(sym_b->n_req, sym_b->n_alloc);

	if (frag_a < frag_b)
		return -1;
	if (frag_a > frag_b)
		return 1;
	return 0;
}

static void show_callers(void) {
	struct symbol_stats **vec;
	size_t size, i;

	if (!callers_flag)
		return;

	vec = ht_malloc_vec(caller_ht, 0);
	size = ht_to_vec(caller_ht, 0, vec);
	qsort(vec, size, sizeof(void *), compare_caller_frag);

	printf("Stats by caller\n");
	printf("Name\tRequested\tAllocated\tFragmentation\n");
	for (i = 0; i < size; i++) {
		printf("%32s\t%10llu\t%10llu\t%.1f%%\n", vec[i]->name,
		       (unsigned long long) vec[i]->n_req,
		       (unsigned long long) vec[i]->n_alloc,
		       fragmentation(vec[i]->n_req, vec[i]->n_alloc));
		free(vec[i]->name);
	}

	ht_free_vec(vec);
}

static int compare_alloc_frag(const void *a, const void *b)
{
	const struct allocation_stats * const alloc_a =
		*((const struct allocation_stats **) a);
	const struct allocation_stats * const alloc_b =
		*((const struct allocation_stats **) b);
	float frag_a = fragmentation(alloc_a->n_req, alloc_a->n_alloc);
	float frag_b = fragmentation(alloc_b->n_req, alloc_b->n_alloc);

	if (frag_a < frag_b)
		return -1;
	if (frag_a > frag_b)
		return 1;
	return 0;
}

static void free_alloc(struct ht_entry *entry, int freeable, void *private)
{
	struct allocation_stats *stats = entry->data;
	
	free(stats->name);

	ht_free_entry(entry, freeable, NULL);
}

static void show_allocations(void) {
	char a2l_result[A2L_QUERY_LEN];
	size_t size, i;
	struct allocation_stats **vec;

	if (!allocs_flag)
		return;

	vec = ht_malloc_vec(alloc_ht, 0);
	size = ht_to_vec(alloc_ht, 0, vec);
	qsort(vec, size, sizeof(void *), compare_alloc_frag);

	printf("Stats by allocation\n");
	printf("Owner\tRequested\tAllocated\tFragmentation\tLifetime\n");
	if (addr2line_flag) {
		for (i = 0; i < size; i++) {
			if (addr2line_query((void *) vec[i]->call_site,
					    a2l_result, A2L_QUERY_LEN) <= 0)
				panic("Couldn't resolve callsite!\n");
			printf("%s\n%32s\t%7llu\t%7llu\t%.1f%%\t",
			       a2l_result, vec[i]->name,
			       (unsigned long long) vec[i]->n_req,
			       (unsigned long long) vec[i]->n_alloc,
			       fragmentation(vec[i]->n_req, vec[i]->n_alloc));
			if (vec[i]->died_on == STILL_ALIVE)
				printf("(alive)\n");
			else
				printf("%lld\n", (long long) vec[i]->died_on -
						 vec[i]->born_on);
		}
	} else {
		for (i = 0; i < size; i++) {
			printf("%32s\t%7llu\t%7llu\t%.1f%%\t", vec[i]->name,
			       (unsigned long long) vec[i]->n_req,
			       (unsigned long long) vec[i]->n_alloc,
			       fragmentation(vec[i]->n_req, vec[i]->n_alloc));
			if (vec[i]->died_on == STILL_ALIVE)
				printf("(alive)\n");
			else
				printf("%lld\n", (long long) vec[i]->died_on -
						 vec[i]->born_on);
		}
	}

	ht_free_vec(vec);
}

static void show_summary(void)
{
	printf("SUMMARY\n=======\n");
	printf("(Ignored erroneous events: %lu)\n", n_ignored);
	printf("Total bytes requested: %lu\n", total_requested);
	printf("Total bytes allocated: %lu\n", total_allocated);
	printf("Total bytes wasted on internal fragmentation: %lu\n",
	       total_allocated - total_requested);
	printf("Internal fragmentation: %f%%\n",
	       fragmentation(total_requested, total_allocated));
	printf("Cross CPU allocations, frees: %lu, %lu\n",
	       n_cross_allocs, n_cross_frees);
}

static inline void show_ht_debug(void)
{
	if (!debug_flag)
		return;

	if (callers_flag)
		ht_show_stats("caller_ht", caller_ht);
	ht_show_stats("alloc_ht", alloc_ht);
}

static void print_usage(void)
{
	printf("kmemtrace reporting tool\nUsage:\n");
	printf("kmemtrace_report [-a | --show-allocs] [-c | --show-callers]\n"
	       "                 [<-p | --prefix> <prefix>] [-d | --debug]\n"
	       "		 [<-s | --vmlinux> <vmlinux>]\n"
	       "kmemtrace_report [-h | --help])\n");
}

static void parse_cmdline(int argc, char **argv)
{
	int c, option_index = 0;
	struct option long_opts[] = {
		{"show-allocs", no_argument, &allocs_flag, 1},
		{"show-callers", no_argument, &callers_flag, 1},
		{"prefix", required_argument, 0, 'p'},
		{"vmlinux", required_argument, 0, 's'},
		{"debug", no_argument, &debug_flag, 1},
		{"help", no_argument, 0, 'h'},
		{0, 0, 0, 0}
	};
	
	for (;;) {
		c = getopt_long(argc, argv, "acdp:s:h",
				long_opts, &option_index);

		if (c == -1)
			break;

		switch (c) {
			case 'a':
				allocs_flag = 1;
				break;
			case 'c':
				callers_flag = 1;
				break;
			case 'd':
				debug_flag = 1;
				break;
			case 'p':
				/* strncpy(prefix, optarg, PREFIX_MAXLEN); */
				break;
			case 's':
				addr2line_flag = 1;
				snprintf(vmlinux_path,
					 FILENAME_MAX, "%s", optarg);
				break;
			case 'h':
				print_usage();
				exit(0);
			default:
				exit(-1);
		}
	}
}

int main(int argc, char **argv)
{
	unsigned long n_cpus = sysconf(_SC_NPROCESSORS_ONLN);
	struct dataset_group *group;

	curr_alloc = calloc(n_cpus, sizeof(unsigned long));

	parse_cmdline(argc, argv);
	
	if (kallsyms_init("kallsyms") <= 0)
		panic("Error while reading kallsyms!\n");
	if (addr2line_flag && addr2line_init(vmlinux_path))
		panic("Could not start addr2line!\n");
	
	alloc_ht = ht_create(12, struct allocation_stats, ptr);
	if (callers_flag)
		caller_ht = ht_create(8, struct symbol_stats, call_site);

	group = dataset_group_create(n_cpus);
	if (dataset_group_add_auto(group, "cpu%lu.out"))
		panic("Could not open input files!\n");
	dataset_iter_ordered(group, parse_event); 
	dataset_group_destroy(group);

	show_callers();
	show_allocations();
	show_summary();
	show_ht_debug();
	
	if (callers_flag) {
		ht_action(caller_ht, ht_free_entry, NULL);
		ht_destroy(caller_ht);
	}
	ht_action(alloc_ht, free_alloc, NULL);
	ht_destroy(alloc_ht);

	if (addr2line_flag)
		addr2line_exit();
	kallsyms_exit();

	free(curr_alloc);

	return EXIT_SUCCESS;
}

