/*
 * Copyright (C) 2008 Eduard - Gabriel Munteanu
 *
 * This file is released under GPL version 2.
 */

#ifndef _DATASET_H
#define _DATASET_H

#include <stdio.h>
#include <errno.h>

struct dataset {
	char			filename[FILENAME_MAX];
	int			fd;
	void			*map;
	size_t			size;
};

struct dataset_event {
	struct kmemtrace_event	*event;
	void			*extra;
};

struct dataset_group {
	struct dataset		**dataset;
	struct dataset_event	*ds_event;
	int			*has_data;
	unsigned long		count;
};

extern struct dataset *dataset_open(char *format, unsigned long cpu);
extern void dataset_close(struct dataset *set);
extern int dataset_iter(struct dataset *set,
			int (*action)(struct kmemtrace_event *, void *));
extern int dataset_iter_ordered(struct dataset_group *group,
				int (*action)(struct kmemtrace_event *,
					      void *, unsigned long));
extern struct dataset_group *dataset_group_create(unsigned long count);
extern void dataset_group_destroy(struct dataset_group *group);

static inline int dataset_group_add(struct dataset_group *group,
				    char *format,
				    unsigned long cpu)
{
	if (cpu >= group->count)
		return -EINVAL;
	
	group->dataset[cpu] = dataset_open(format, cpu);
	if (!group->dataset[cpu])
		return -ENOMEM;
	
	return 0;
}

static inline int dataset_group_add_auto(struct dataset_group *group,
					 char *format)
{
	unsigned long cpu;
	int err;

	for (cpu = 0; cpu < group->count; cpu++) {
		err = dataset_group_add(group, format, cpu);
		if (err)
			return -ENOMEM;
	}
	
	return 0;
}

#endif /* _DATASET_H */

